import { Geocalisation } from "../core/Geocalisation";
import { Lap } from "../core/Lap";
import { Member } from "../core/Member";
import { Owner } from "../core/Owner";
import { Point } from "../core/Point";
import { Session } from "../core/Session";
import { SessionType } from "../core/SessionType";
import { Team } from "../core/Team";
import { User } from "../core/User";
import { WaitingMember } from "../core/WaitingMember";

const owner1 = new Owner("eric","PASSWORD123","eric@gmail.com",[]);
const owner2 = new Owner("castor","PASSWORD123","castor@gmail.com",[]);

export const USERS1 : User[] = [
    new Member("jean","PASSWORD123","jean@gmail.com",[]),
    new WaitingMember("anas","PASSWORD123","anas@gmail.com",[]),
] 
export const USERS2 : User[] = [
    new Member("stickman","PASSWORD123","stickman@gmail.com",[]),
    new Member("crapaud","PASSWORD123","crapaud@gmail.com",[]),
    new WaitingMember("mcdo","PASSWORD123","mcdo@gmail.com",[]),
    new WaitingMember("bucheron","PASSWORD123","bucheron@gmail.com",[]),
]

export const TEAMS : Team[] = [
    new Team("La team 1",owner1, USERS1),
    new Team("Bat INFO",owner2,USERS2)
]


const geo1 = new Geocalisation(40.7128, -74.0060);
const geo2 = new Geocalisation(51.5074, -0.1278);
const geo3 = new Geocalisation(35.6895, 139.6917);


const point1 = new Point(geo1, 120, 10, 3, 50, 90, 20, 10, 20, 100);
const point2 = new Point(geo2, 130, 15, 4, 60, 100, 30, 15, 25, 120);
const point3 = new Point(geo3, 140, 20, 5, 70, 110, 40, 20, 30, 140);


const lap1 = new Lap(1, [point1, point2], 250);
const lap2 = new Lap(2, [point2, point3], 300);
const lap3 = new Lap(3, [point3, point1], 350);


const session1 = new Session("First Session", [lap1, lap2], SessionType.PrivateTest);
const session2 = new Session("Second Session", [lap2, lap3], SessionType.Qualification);
const session3 = new Session("Third Session", [lap3, lap1], SessionType.Unknown);

export const SESSIONS : Session[] = [ 
    session1,session2,session3
]