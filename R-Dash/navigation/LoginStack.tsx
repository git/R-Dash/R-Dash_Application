import { createStackNavigator } from "@react-navigation/stack";
import Login from "../screens/Login";
import Register from "../screens/Register";
import Navigation from "./Navigation";

export default function LoginStack() {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator initialRouteName="SignIn" screenOptions={{ headerShown: false }}>
            <Stack.Screen name="SignIn" component={Login} />
            <Stack.Screen name="SignUp" component={Register} />
            <Stack.Screen name="Home" component={Navigation} />
        </Stack.Navigator>
    )
}