import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import { FontAwesome } from '@expo/vector-icons';
import MainStack from "./MainStack";
import TeamStack from "./TeamStack";


export default function Navigation() {
    const BottomTabNavigator = createBottomTabNavigator();
    return (
        <NavigationContainer independent={true}>
            <BottomTabNavigator.Navigator initialRouteName="Home" screenOptions={{ headerShown: false }}>
                <BottomTabNavigator.Screen name="Home" component={MainStack}
                    options={{
                        title: 'Home',
                        tabBarIcon: ({ color }) => <FontAwesome name="home" size={40} color={color} />
                    }} />

                <BottomTabNavigator.Screen name="Team" component={TeamStack}
                    options={{
                        title: 'Team',
                        tabBarIcon: ({ color }) => <FontAwesome name="group" size={24} color={color} />
                    }}/>

            </BottomTabNavigator.Navigator> 
        </NavigationContainer>    
    )
}