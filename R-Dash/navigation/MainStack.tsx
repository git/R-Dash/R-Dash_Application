import { createStackNavigator } from "@react-navigation/stack";
import Lap from "../screens/Lap";
import NewTrack from "../screens/NewTrack";
import Point_Viewer from "../screens/Point_Viewer";
import Session_browser from "../screens/Session_browser";
import SettingsStack from "./SettingsStack";

export default function MainStack() {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator initialRouteName="Home" screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Home" component={Session_browser} />
            <Stack.Screen name="Add" component={NewTrack} />
            <Stack.Screen name="Lap" component={Lap} />
            <Stack.Screen name="PointInfo" component={Point_Viewer} />
            <Stack.Screen name="Settings" component={SettingsStack} />
        </Stack.Navigator>
    )
}