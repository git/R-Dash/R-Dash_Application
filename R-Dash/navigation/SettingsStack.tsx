import { createStackNavigator } from "@react-navigation/stack";
import ChangePassword from "../screens/ChangePassword";
import ManageAccount from "../screens/ManageAccount";


export default function SettingsStack() {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator initialRouteName="Home" screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Home" component={ManageAccount} />
            <Stack.Screen name="Password" component={ChangePassword} />
        </Stack.Navigator>
    )
}