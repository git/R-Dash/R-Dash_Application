import { createStackNavigator } from "@react-navigation/stack";
import CreateTeam from "../screens/CreateTeam";
import Team_Selection from "../screens/Team_Selection";
import TeamInfoStack from "./TeamInfoStack";


export default function TeamStack() {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator initialRouteName="Home" screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Home" component={Team_Selection} />
            <Stack.Screen name="Browse" component={TeamInfoStack} />
            <Stack.Screen name="Create" component={CreateTeam} />
        </Stack.Navigator>
    )
}