import { createStackNavigator } from "@react-navigation/stack";
import CreateTeam from "../screens/CreateTeam";
import Team_Info from "../screens/Team_Info";
import Team_Browser from "../screens/Team_Browser";
import Team_Selection from "../screens/Team_Selection";


export default function TeamInfoStack() {
    const Stack = createStackNavigator();
    return (
        <Stack.Navigator initialRouteName="Home" screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Home" component={Team_Browser} />
            <Stack.Screen name="Info" component={Team_Info} />
        </Stack.Navigator>
    )
}