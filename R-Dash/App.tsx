import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import LoginStack from './navigation/LoginStack';
import Navigation from './navigation/Navigation';
import ChangePassword from './screens/ChangePassword';
import CreateTeam from './screens/CreateTeam';
import Team_Info from './screens/Team_Info';
import Login from './screens/Login';
import ManageAccount from './screens/ManageAccount';
import NewTrack from './screens/NewTrack';
import Register from './screens/Register';
import Session_browser from './screens/Session_browser';
import Team_Browser from './screens/Team_Browser';
import Team_Selection from './screens/Team_Selection';
import { Provider } from 'react-redux';
import store from './redux/Store';

export default function App() {
    return (
        <Provider store={store}>
            <SafeAreaView style={styles.container}>
                <NavigationContainer independent={true}>
                    <LoginStack />
                </NavigationContainer>
            </SafeAreaView>
        </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
