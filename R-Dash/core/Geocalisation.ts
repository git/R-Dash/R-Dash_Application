export class Geocalisation {
    private gpsLat: number;
    private gpsLong: number;

    constructor(gpsLat: number, gpsLong: number) {
        this.gpsLat = gpsLat;
        this.gpsLong = gpsLong;
    }

    getGpsLat() {
        return this.gpsLat;
    }

    setGpsLat(gpsLat: number) {
        this.gpsLat = gpsLat;
    }

    getGpsLong() {
        return this.gpsLong;
    }

    setGpsLong(gpsLong: number) {
        this.gpsLong = gpsLong;
    }
}