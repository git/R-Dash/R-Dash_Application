import { Point } from "./Point";

export class Lap {
    private number : number;
    private points: Point[];
    private time: string;

    constructor(number: number,points: Point[], time: string) {
        this.number = number;
        this.points = points;
        this.time = time;
    }

    getNumber(){
        return this.number;
    }
    setNumber(number :number){
        this.number = number;
    }

    getPoints() {
        return this.points;
    }
    
    setPoints(points: Point[]) {
        this.points = points;
    }

    getTime() {
        return this.time;
    }
    
    getFormattedTime(){
        // const timeInSeconds = this.time;
        // const timeInMs = timeInSeconds * 1000;
        // const date = new Date(timeInMs);
        // const minutes = date.getMinutes().toString().padStart(2, "0");
        // const seconds = date.getSeconds().toString().padStart(2, "0");
        // const milliseconds = date.getMilliseconds().toString().padStart(2, "0");
        // const formattedTime = `${minutes}:${seconds}:${milliseconds}`;
        // return formattedTime;
    }

    setTime(time: string) {
        this.time = time;
    }
    getAverageSpeed(){
        var sum = 0;
        this.points.forEach(pt => {
            sum += pt.getVCar();
        });
        return sum/this.points.length;
    }

    getMaxSpeed(){
        var max = 0.0;
        this.points.forEach(pt => {
            if(max < pt.getVCar()){
                max = pt.getVCar();
            }
        });
        return max;
    }

    getLocationLat(){
        var sum = 0.0;
        this.points.forEach(pt => {
            sum += pt.getGeo().getGpsLat();
        });
        return sum/this.points.length;
    }

    getLocationLong(){
        var sum = 0.0;
        this.points.forEach(pt => {
            sum += pt.getGeo().getGpsLong();
        });
        return sum/this.points.length;
    }

}