export enum SessionType {
    Test = "Test",
    PrivateTest = "Private Test",
    Qualification = "Qualification",
    ShortStroke = "Short Stroke",
    LongStroke = "Long Stroke",
    Unknown = "Unknown",
  }