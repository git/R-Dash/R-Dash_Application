import { Lap } from "./Lap";
import { SessionType } from "./SessionType";

export class Session {
    private name: string;
    private type: SessionType;
    private laps: Lap[];

    constructor( name: string, laps: Lap[], type: SessionType = SessionType.Unknown) {
        this.type = type
        this.name = name;
        this.laps = laps;
    }
    getType() {
        return this.type;
    }

    setType(type: SessionType) {
        this.type = type;
    }
    
    getName() {
        return this.name;
    }

    setName(name: string) {
        this.name = name;
    }
    getLaps() {
        return this.laps;
    }

    setLaps(laps: Lap[]) {
        this.laps = laps;
    }
}