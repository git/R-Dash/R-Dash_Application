import { Session } from "./Session";


export interface ILoader{
    Load() : Session
    LoadFromFile(file : File) : Session
}