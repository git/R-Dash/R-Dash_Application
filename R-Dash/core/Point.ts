import { Geocalisation } from "./Geocalisation";

export class Point {
    private geo: Geocalisation;
    private timer: number;
    private distance: number;
    private nGear: number;
    private pBrakeF: number;
    private aSteer: number;
    private rPedal: number;
    private gLong: number;
    private gLat: number;
    private vCar: number;

    constructor( geo: Geocalisation, timer: number, distance: number, nGear: number, pBrakeF: number, aSteer: number, rPedal: number, gLong: number, gLat: number, vCar: number) {
        this.geo = geo;
        this.timer = timer;
        this.distance = distance;
        this.nGear = nGear;
        this.pBrakeF = pBrakeF;
        this.aSteer = aSteer;
        this.rPedal = rPedal;
        this.gLong = gLong;
        this.gLat = gLat;
        this.vCar = vCar;
    }
    
    getGeo() {
        return this.geo;
    }

    setGeo(geo: Geocalisation) {
        this.geo = geo;
    }

    setTimer(timer: number) {
        this.timer = timer;
    }

    getTimer() {
        return this.timer;
    }

    getFormattedTime(){
        const timeInSeconds = this.timer;
        const timeInMs = timeInSeconds * 1000;
        const date = new Date(timeInMs);
        const minutes = date.getMinutes().toString().padStart(2, "0");
        const seconds = date.getSeconds().toString().padStart(2, "0");
        const milliseconds = date.getMilliseconds().toString().padStart(2, "0");
        const formattedTime = `${minutes}:${seconds}:${milliseconds}`;
        return formattedTime;
    }

    getDistance() {
        return this.distance;
    }

    setDistance(distance: number) {
        this.distance = distance;
    }

    getNGear() {
        return this.nGear;
    }

    setNGear(nGear: number) {
        this.nGear = nGear;
    }

    getPBreakF() {
        return this.pBrakeF;
    }

    setPBreakF(pBreakF: number) {
        this.pBrakeF = this.pBrakeF;
    }

    getASteer() {
        return this.aSteer;
    }

    setASteer(aSteer: number) {
        this.aSteer = aSteer;
    }

    getRPedal() {
        return this.rPedal;
    }

    setRPedal(rPedal: number) {
        this.rPedal = rPedal;
    }

    getGLong() {
        return this.gLong;
    }

    setGLong(gLong: number) {
        this.gLong = gLong;
    }

    getGLat() {
        return this.gLat;
    }

    setGLat(gLat: number) {
        this.gLat = gLat;
    }

    getVCar() {
        return this.vCar;
    }

    setVCar(vCar: number) {
        this.vCar = vCar;
    }
}