import { Session } from "./Session";
import { Team } from "./Team";
import { User } from "./User";

export class Owner extends User {
    constructor( username: string, password: string, email: string, sessions: Session[],image : HTMLImageElement= require('../assets/images/user.jpg')) {
        super(username, password, email, sessions,image);
    }

    IsOwner(): boolean{
        return true;
    }
}