import { User } from "./User";
import { WaitingMember } from "./WaitingMember";

export class Team {
    private name: string;
    private owner: User;
    private users: User[];
    private logo: HTMLImageElement;

    constructor(name: string, owner: User ,users: User[] = [], logo: HTMLImageElement = require('../assets/images/people.jpg')) {
        this.name = name;
        this.owner = owner;
        this.users = users;
        this.logo = logo;
    }

    getName() {
        return this.name;
    }

    setName(name: string) {
        this.name = name;
    }

    getOwner() {
        return this.owner;
    }

    setOwner(owner: User) {
        this.owner = owner;
    }

    getusers() {
        return this.users;
    }

    setusers(users: User[]) {
        this.users = users;
    }

    getLogo() {
        return this.logo;
    }

    setLogo(logo: HTMLImageElement) {
        this.logo = logo;
    }

    getWaitList() {
        var waitList : User[] = [];
        this.users.forEach(user => {
            if(user as WaitingMember){
                waitList.concat(user);
            }
        });
        return waitList;
    }

    addToWaitList(user: User) : boolean{
        if(!this.users.includes(user)){
            this.users.concat(user);
            return true;
        }
        return false;
    }
}