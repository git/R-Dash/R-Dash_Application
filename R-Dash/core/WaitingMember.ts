import { Session } from "./Session";
import { User } from "./User";

export class WaitingMember extends User {
    constructor( pseudo: string, password: string, email: string, sessions: Session[],image : HTMLImageElement= require('../assets/images/user.jpg')) {
        super(pseudo, password, email, sessions, image);
    }
}