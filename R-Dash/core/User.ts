import { Session } from "./Session";
import { Team } from "./Team";

export abstract class User {
    private username: string;
    private password: string;
    private email: string;
    private sessions: Session[];
    private image : HTMLImageElement;

    constructor(username: string, password: string, email: string, sessions: Session[] = [], image : HTMLImageElement) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.sessions = sessions;
        this.image = image;

    }

    getImage(){
        return this.image;
    }

    setImage(image: HTMLImageElement){
        this.image = image;
    }

    getUsername() {
        return this.username;
    }

    setUsername(username: string) {
        this.username = username;
    }

    getPassword() {
        return this.password;
    }

    setPassword(password: string) {
        this.password = password;
    }

    getEmail() {
        return this.email;
    }

    setEmail(email: string) {
        this.email = email;
    }

    getSessions() {
        return this.sessions;
    }

    setSessions(sessions: Session[]) {
        this.sessions = sessions;
    }

}