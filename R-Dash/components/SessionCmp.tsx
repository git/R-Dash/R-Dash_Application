import React from "react";
import { View,Image,Text, StyleSheet, Touchable } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Session } from "../core/Session";

type SessionListItemProps = {
    session: Session;
    onPress: (team: Session) => void;
  }
  
  export default function SessionListItem(props: SessionListItemProps) {

    const timeInSeconds = props.session.getLaps()[props.session.getLaps().length - 1].getTime();
    return (
      <TouchableOpacity style={styles.container} 
                        onPress={()=> props.onPress(props.session)}>
        <Text style={styles.text_style}> {props.session.getName()}</Text>
        <View style={styles.container_low}>
             <Text style={styles.text_under}> {props.session.getType().toString() }</Text>
            <Text style={styles.text_under}> { timeInSeconds }</Text>
        </View>

      </TouchableOpacity>
    )
  }
  
  const styles = StyleSheet.create({
    container: {
        borderRadius:30,
        alignItems:'center',
        backgroundColor:'#e1e8ee',
        padding:1,
        flex: 1,
        margin:10,
    },
    container_low: {
        borderRadius:30,
        alignItems:'center',
        backgroundColor:'#e1e8ee',
        padding:10,
        flex: 1,
        flexDirection: "row",
        marginBottom:10,
    },
    text_style:{
        textAlign:'center',
        fontSize:25,
    },
    text_under:{
        marginLeft:15,
        textAlign:'center',
        fontSize:20,
    }
  });
  