import React from "react";
import { FlatList, StyleSheet, Text, View, Image, TouchableOpacity, TouchableHighlight } from 'react-native';


export default function TopBar(props: { navigation: any }) {
    const { navigation } = props;

    return (
        <View style={styles.topbar}>
            <View>
                <TouchableOpacity style={styles.accountContainerButton} onPress={() => navigation.navigate("Settings")}>
                    <View style={styles.ImageContainer}>
                        <Image source={require('../assets/images/pfp.png')} style={styles.accountImage} />
                    </View>
                    <Text style={styles.accountName}>Mr X</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    topbar: {
        backgroundColor: '#BF181D',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        height: '10%',
    },

    accountContainerButton: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    ImageContainer: {
        backgroundColor: '#fff',
        borderRadius: 50,
        width: 40,
        height: 40,
        overflow: 'hidden',
        marginRight: 10,
    },

    accountImage: {
        width: '100%',
        height: '100%',
    },

    accountName: {
        color: '#fff',
        fontSize: 20,
    },

    topbarRight: {
        flex: 1,
        alignItems: 'flex-end',
    },

    iconContainer: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        backgroundColor: 'transparent',
        width: 24,
        height: 24,
    },

    accountSettings: {
        width: '100%',
        height: '100%',
    },
})