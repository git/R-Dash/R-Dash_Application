import React from "react";
import { View,Image,Text, StyleSheet, Touchable } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Team } from "../core/Team";

type TeamListItemProps = {
    team: Team;
    onPress: (team: Team) => void;
  }
  
  export default function TeamListItem(props: TeamListItemProps) {
    return (
      <TouchableOpacity style={styles.container} 
                        onPress={()=> props.onPress(props.team)}>
        <Image style={styles.teaserImage} source={props.team.getLogo()} />
        <Text style={styles.text_style}> {props.team.getName()}</Text>
      </TouchableOpacity>
    )
  }
  
  const styles = StyleSheet.create({
    container: {
        alignItems:'center',
        backgroundColor:'#e1e8ee',
        padding:10,
        flex: 1,
        flexDirection: "row",
        marginBottom:10,
    },
    text_style:{
        marginLeft:15,
        textAlign:'center',
        fontSize:20,
    },
    teaserImage: {
        borderWidth:1,
        borderColor:'black',
        borderRadius:50,
        width: 50,
        height: 50,
    }
  });
  