import React from 'react';
import { Pressable } from 'react-native';
import { StyleSheet, Text, View, Image } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function Team_Info(props: { navigation: any }) {
    const { navigation } = props;
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.maincard}>
                <Text style={{ paddingBottom: 10, fontSize: 30 }} >Team Information</Text>
                <View style={styles.card}>
                    <Image
                        style={styles.logo}
                        source={require('../assets/images/image.png')}
                    />
                    <View >
                        <Text style={{ padding: 10 }}>Name : </Text>
                        <Text style={{ padding: 10 }}>Owner : </Text>
                        <Pressable style={styles.button}>
                            <Text style={styles.button_text}>Request to join</Text>
                        </Pressable>
                    </View>
                </View>
                <Pressable style={styles.button} onPress={() => navigation.goBack() }>
                    <Text style={styles.button_text}>Retour</Text>
                </Pressable>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#C5C5C5',
        alignItems: 'center',
        justifyContent: 'center',
    },
    maincard: {
        height: "60%",
        width: "80%",
        backgroundColor: "#fff",
        borderRadius: 15,
        padding: 10,
        elevation: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    card: {
        height: "60%",
        width: "80%",
        backgroundColor: "#C8C8C8",
        padding: 10,
        elevation: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {

        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 10,
        elevation: 3,
        backgroundColor: '#BF181D',
    },
    button_text: {
        color: '#fff',
        fontSize: 15,
    },
    logo: {
        borderRadius: 100,
        width: 100,
        height: 100,
    },
}); 
