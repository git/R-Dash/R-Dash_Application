import React from 'react';
import { Pressable } from 'react-native';
import { StyleSheet, Text, View, Image, TextInput } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function Register(props: { navigation: any }) {
    const { navigation } = props;
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.maincard}>
                <Text style={{ paddingBottom: 10, fontSize: 30 }}>Register</Text>
                <View style={styles.input}>
                    <TextInput
                        placeholder="Name"
                    />
                    <Image
                        style={styles.logo}
                        source={require('../assets/images/user.png')}
                    />
                </View>
                <View style={styles.input}>
                    <TextInput

                        keyboardType="email-address"
                        placeholder="Email"
                    />
                    <Image
                        style={styles.logo}
                        source={require('../assets/images/email.png')}
                    />
                </View>
                <View style={styles.input}>
                    <TextInput
                        secureTextEntry={true}
                        placeholder="Password"
                    />
                    <Image
                        style={styles.logo}
                        source={require('../assets/images/cadna.png')}
                    />
                </View>
                <View style={styles.input}>
                    <TextInput
                        secureTextEntry={true}
                        placeholder="Confirm Password"
                    />
                    <Image
                        style={styles.logo}
                        source={require('../assets/images/cadna.png')}
                    />
                </View>
                <Pressable style={styles.button} onPress={() => navigation.navigate('Home')}>
                    <Text style={styles.text}>Sign Up</Text>
                </Pressable>
                <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                    <Text style={styles.text}>Already a Member ? </Text>
                    <Pressable onPress={() => navigation.replace('SignIn')} >
                        <Text style={styles.register}>Sign in</Text>
                    </Pressable>
                </View>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#C5C5C5',
        alignItems: 'center',
        justifyContent: 'center',
    },
    maincard: {
        height: "60%",
        width: "80%",
        backgroundColor: "#fff",
        borderRadius: 15,
        padding: 20,
        elevation: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    input: {
        height: "auto",
        width: 300,
        borderRadius: 10,
        backgroundColor: "#C8C8C8",
        padding: 10,
        elevation: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 10,
        elevation: 3,
        backgroundColor: '#BF181D',
    },
    text: {
        color: '#000',
        fontSize: 15,
    },
    logo: {
        width: 30,
        height: 30,
    },
    register: {
        color: '#ffa020',
        fontSize: 15,
        textDecorationLine: 'underline',
    }
});