import { useState } from 'react';
import React from 'react';
import { Pressable } from 'react-native';
import { StyleSheet, Text, View, Image, TextInput } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
// import { AsyncStorage } from 'react-native';


export default function Login(props: { navigation: any }) {
    const { navigation } = props;

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    var [errorMessage, setMessage] = useState('');
    // MANQUE REDUX POUR STOCKER LA VALEUR EMAIL DANS L'APPLI

    const handleEmailChange = (text: string) => {
        setEmail(text);
        setMessage('');
    }

    const handlePasswordChange = (text: string) => {
        setPassword(text);
        setMessage('');
    }
    const secretKey = 'SECRET'; // appele API pour récupérer une clé secrete

    const handleLogin = () => {
        const isCorrect = true // envoi email + password a l'API, retour bool de l'API
         // If the email and password are correct, generate a JWT

        if(isCorrect){
           //const token = jwt.sign({ email }, secretKey);
            // Store the token in AsyncStorage for persistent storage
           // AsyncStorage.setItem('token', token);
            setMessage('');
            navigation.navigate('Home');
        }
        else{
            // Handle incorrect email or password
            setMessage('Bad credentials. Please login again');

        }
    }
    
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.maincard}>
                <Text style={{ paddingBottom: 10, fontSize: 30 }} >Login</Text>
                <Text style={{ paddingBottom: 10, fontSize: 15, color:'red'}} >{ errorMessage }</Text>
                <View style={styles.inputContainer}>
                    <TextInput 
                        style={styles.email}
                         keyboardType="email-address"
                         placeholder="Email"
                         value={email}
                         onChangeText={handleEmailChange}
                    />
                    {/* <Image
                        style={styles.logo}
                        source={require('../assets/images/email.png')}
                    /> */}
                </View>
                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.email}
                        secureTextEntry={true}
                        placeholder="Password"
                        value={password}
                        onChangeText={handlePasswordChange}
                    />
                    {/* <Image
                        style={styles.logo}
                        source={require('../assets/images/cadna.png')}
                    /> */}
                </View>
                <Pressable style={styles.button} onPress={handleLogin}>
                    <Text style={styles.text}>Login</Text>
                </Pressable>
                <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                    <Text style={styles.text}>Don't have Account ? </Text>
                    <Pressable onPress={() => navigation.replace('SignUp')}>
                        <Text style={styles.register}>Register</Text>
                    </Pressable>
                </View>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    inputContainer:{
        flexDirection:'row',
    },
    container: {
        flex: 1,
        backgroundColor: '#C5C5C5',
        alignItems: 'center',
        justifyContent: 'center',
    },
    maincard: {
        height: "40%",
        width: "80%",
        backgroundColor: "#fff",
        borderRadius: 15,
        padding: 20,
        elevation: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    email: {
        height: "auto",
        width: 300,
        borderRadius: 10,
        backgroundColor: "#C8C8C8",
        padding: 10,
        elevation: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 10,
        elevation: 3,
        backgroundColor: '#BF181D',
    },
    text: {
        color: '#000',
        fontSize: 15,
    },
    logo: {
        width: 30,
        height: 30,
    },
    register: {
        color: '#ffa020',
        fontSize: 15,
        textDecorationLine: 'underline',
    }
});