import React, { useState } from 'react';
import { Pressable } from 'react-native';
import { StyleSheet, Text, View, Image, TextInput } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import * as DocumentPicker from 'expo-document-picker';

export default function ManageAccount(props: { navigation: any }) {
    const { navigation } = props;

    const [pickedDocument, setPickedDocument] = useState<DocumentPicker.DocumentResult | null>(null);

    const handlePickDocument = async () => {
      try {
        const result = await DocumentPicker.getDocumentAsync({ type: ['image/png', 'image/jpeg', 'image/jpg'] });
        if (result.type === 'success') {
          setPickedDocument(result);
        }
      } catch (err) {
        console.log(err);
      }
    };

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.maincard}>
                <Text style={{ paddingBottom: 10, fontSize: 30 }}>Manage account</Text>
                <View style={styles.card}>
                    <View style={styles.placement}>
                        <Image style={styles.logo} source={require('../assets/images/image.png')} />
                        
                        <Pressable onPress={handlePickDocument}>
                            <Image style={styles.import} source={require('../assets/images/import.png')} />
                            <Text>
                                Picked document: {pickedDocument ? pickedDocument.name : 'none'}
                            </Text>
                        </Pressable>
                    </View>

                    <View>
                        <Text>Name : </Text>
                        <TextInput style={styles.nameCard} secureTextEntry={true} placeholder="Name" />
                    </View>

                    <Pressable style={styles.button} onPress={() => navigation.navigate('Password')}>
                        <Text>Change password</Text>
                    </Pressable>

                    <View style={styles.placement}>
                        <Pressable style={styles.button} onPress={() => navigation.goBack()}>
                            <Image style={styles.return} source={require('../assets/images/return.png')} />
                        </Pressable>
                        <Pressable style={styles.button} onPress={() => navigation.goBack()}>
                            <Image style={styles.return} source={require('../assets/images/checked.png')} />
                        </Pressable>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    );
};
    const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: '#C5C5C5',
            alignItems: 'center',
            justifyContent: 'center',
        },
        maincard: {
            height: "60%",
            width: "80%",
            backgroundColor: "#fff",
            borderRadius: 15,
            padding: 10,
            elevation: 10,
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 3 },
            shadowOpacity: 0.5,
            shadowRadius: 5,
            justifyContent: 'space-evenly',
            alignItems: 'center',
        },
        card: {
            height: "70%",
            width: "80%",
            backgroundColor: "#C8C8C8",
            padding: 10,
            elevation: 10,
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 3 },
            shadowOpacity: 0.5,
            shadowRadius: 5,
            justifyContent: 'space-evenly',
            paddingHorizontal: 20,
        },
        button: {

            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 12,
            paddingHorizontal: 32,
            borderRadius: 10,
            elevation: 3,
            backgroundColor: '#BF181D',
        },
        button_text: {
            color: '#fff',
            fontSize: 15,
        },
        logo: {
            borderRadius: 100,
            width: 100,
            height: 100,
        },
        import: {
            width: 70,
            height: 70,
            paddingLeft: 20,
        },
        placement: {
            justifyContent: 'space-between',
            flexDirection: 'row',
        },
        nameCard: {
            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 12,
            paddingHorizontal: 32,
            elevation: 3,
            backgroundColor: '#fff',
            borderWidth: 1,
            borderColor: '#000',
        },
        return: {
            width: 30,
            height: 30,
        }
    });