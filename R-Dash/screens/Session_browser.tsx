import { BackgroundImage, SearchBar } from "@rneui/base";
import React, { useEffect, useState } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import SessionListItem from "../components/SessionCmp";
import TopBar from "../components/TopBar";
import { Session } from "../core/Session";
import { getSessionsList } from "../redux/actions/sessions";
import { SESSIONS } from "../stub/stub";

export default function Session_browser(props: { navigation: any }) {
  const { navigation } = props;
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(false);

  const handlePress = (item: Session) => {
    setSearch("");
    navigation.navigate("Lap", { "session" : item });
  };


    const sessions = useSelector(state => state.appReducer.sessions);

    const dispatch = useDispatch();
    useEffect(() => {
      setLoading(true);
        const loadTeams = async () => {
            await dispatch(getSessionsList());
            setLoading(false);
        };
        loadTeams();
    }, [dispatch]);

    const filteredData =
    search !== ""
      ? sessions.filter((item) =>
          item.getName().toLowerCase().includes(search.toLowerCase())
        )
      : sessions;
  return (
    <View style={styles.container}>
      <View style={styles.container}>
        {/* Header */}
        <TopBar navigation={navigation} />

        {/* Body */}
        <View style={styles.container}>
          <BackgroundImage
            source={require("../assets/images/rdash.png")}
            resizeMode="contain"
            style={styles.backgroundImage}
          >

            <Text style={styles.text_title}>Sessions</Text>
            <SearchBar
              platform="default"
              lightTheme
              value={search}
              onChangeText={setSearch}
            />
            {loading ? (
              <ActivityIndicator size="large" color="#BF181F" />
            ) : (
              <FlatList
                data={filteredData}
                renderItem={({ item }) => (
                  <SessionListItem session={item} onPress={handlePress} />
                )}
                keyExtractor={(Item) => Item.getName()}
              />
            )}

            <TouchableOpacity
              style={styles.addContainerButton}
              onPress={() => navigation.navigate("Add")}
            >
              <View>
                <Text style={styles.button_text}>Add a session</Text>
              </View>
            </TouchableOpacity>
          </BackgroundImage>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  item: {
    borderRadius: 10,
    opacity: 0.9,
    backgroundColor: "#C5C5C5",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  backgroundImage: {
    flex: 1,
    width: "100%",
  },

  container: {
    flex: 1,
  },

  addImageContainer: {
    backgroundColor: "#fff",
    borderRadius: 50,
    width: 40,
    height: 40,
    overflow: "hidden",
    alignItems: "center",
  },

  addContainerButton: {
    margin: 10,
    alignItems: "center",
    justifyContent: "center",
    height: "10%",
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 10,
    elevation: 3,
    backgroundColor: "#BF181F",
  },
  button_text: {
    color: "#fff",
    fontSize: 20,
  },

  text_title: {
    fontWeight: "bold",
    fontSize: 30,
    textAlign: "center",
    padding: 10,
    textShadowColor: "#fff",
    textShadowOffset: { width: 0, height: 0 },
    textShadowRadius: 10,
  },

  topbar: {
    height: "10%",
  },
});
