import { BackgroundImage } from '@rneui/base';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import MapView, { Polyline } from 'react-native-maps';
import React from 'react';
import { Lap as LapModel}   from '../core/Lap';
import TopBar from '../components/TopBar';

export default function Lap(props: { navigation: any, route : any}) {
    const { session } = props.route.params;
    const { navigation } = props;

    const laps: LapModel[] = session.getLaps().sort((lap1 : LapModel, lap2 : LapModel) => lap1.getNumber() - lap2.getNumber());


    const [currentLapIndex, setCurrentLapIndex] = React.useState(0);
    const currentLap: LapModel | undefined = laps[currentLapIndex];

    const goToPreviousLap = () => {
        if (currentLapIndex > 0) {
            setCurrentLapIndex(currentLapIndex - 1);
        }
    };

    const goToNextLap = () => {
        if (currentLapIndex < laps.length - 1) {
            setCurrentLapIndex(currentLapIndex + 1);
        }
    };

    const points : any = [];
    currentLap.getPoints().forEach(pt => {
        points.push({ latitude: pt.getGeo().getGpsLat(), longitude: pt.getGeo().getGpsLong() });
            
    });

    //var currentLap : undefined; // Penser à vérifier si la session ne possède pas de tour !! si c'est le cas afficher une popup d'erreur
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                {/* Header */}
                <TopBar navigation={navigation} />
                
                {/* Body */}
                <View style={styles.container}>

                        <View style={styles.top_lap}>
                            <TouchableOpacity style={[styles.LapBrowserButton, currentLapIndex === 0 ? styles.disabled : null]} onPress={goToPreviousLap}>
                                <View>
                                    <Text style={styles.button_text} >Previous lap</Text>
                                </View>
                            </TouchableOpacity>

                            <Text style={styles.text_title}>Lap {currentLap?.getNumber()} </Text>

                            <TouchableOpacity style={[styles.LapBrowserButton, currentLapIndex === laps.length - 1 ? styles.disabled : null]} onPress={goToNextLap}>
                                <View>
                                    <Text style={styles.button_text} >Next lap</Text>
                                </View>
                            </TouchableOpacity>

                        </View>

                        <View style={styles.containerBody}>
                            <TouchableOpacity
                                style={styles.addContainerButton}
                                onPress={() => navigation.navigate("PointInfo",{"currentLap": currentLap})}
                                >
                                <View>
                                    <Text style={styles.button_text}>Point Viewer Mode</Text>
                                </View>
                            </TouchableOpacity>
                            <MapView
                                style={styles.map}
                                mapType="satellite"
                                region={{
                                    latitude:  currentLap.getLocationLat(),
                                    longitude: currentLap.getLocationLong(),
                                    latitudeDelta: 0.001,
                                    longitudeDelta: 0.015,
                                }}
                            >
                                <Polyline
                                    coordinates={points}
                                    strokeColor="red" // set the color of the line
                                    strokeWidth={3} // set the width of the line
                                    />
                                {/* <Marker
                                    coordinate={{
                                        latitude: 45.76013484856403,
                                        longitude: 3.112214188782891,
                                    }}
                                    onPress={() => console.log('pressed')}
                                /> */}

                            </MapView>
                            <BackgroundImage source={require('../assets/images/rdash.png')} resizeMode="contain" >
                                <View style={styles.container2}>

                                <Text style={styles.header}>Lap Information</Text>
                                
                                <View style={styles.infoContainer}>
                                    <Text style={styles.infoItem}>Lap Time:</Text>
                                    <Text style={styles.infoValue}>{currentLap.getTime()}</Text>
                                </View>

                                <View style={styles.infoContainer}>
                                    <Text style={styles.infoItem}>Average Speed:</Text>
                                    <Text style={styles.infoValue}>{currentLap.getAverageSpeed().toFixed()} km/h</Text>
                                </View>

                                <View style={styles.infoContainer}>
                                    <Text style={styles.infoItem}>Max Speed:</Text>
                                    <Text style={styles.infoValue}>{currentLap.getMaxSpeed().toFixed()} km/h</Text>
                                </View>
                                </View>
                            </BackgroundImage>
                        </View>
                </View>

            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    disabled: {
        opacity: 0.5,
        },
    addContainerButton: {
        margin:5,
        alignItems: "center",
        justifyContent: "center",
        height: "8%",
        borderRadius: 10,
        elevation: 3,
        backgroundColor: "#BF181F",
      },
    containerBody:{
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'stretch',
    },
    container2: {
        backgroundColor: 'rgba(255, 255, 255, 0.9)',
        borderColor:'black',
        borderWidth:2,
        padding: 10,
        borderRadius: 10,
        marginBottom: 20,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
            },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    header: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    infoContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 5,
    },
    infoItem: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    infoValue: {
        fontSize: 18,
    },
    map: {
        flex:1,
    },
    top_lap: {
        height: "8%",
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    backgroundImage: {
        flex: 1,
        width: '100%',
    },
    container: {
        flex: 1,
    },
    LapBrowserButton: {
        width: "30%",
        margin: 5,
        alignContent: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        backgroundColor: '#BF181F',
    },
    button_text: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 20,
    },
    text_title: {
        fontSize: 30,
        textAlign: 'center',
    },

});