import React from 'react';
import { Pressable, StyleSheet, Text, View } from 'react-native';
import TopBar from '../components/TopBar';

export default function Team_Selection(props: { navigation: any }) {
    const { navigation } = props;
    return (
        <View style={styles.main}>
            <TopBar navigation={navigation}/>
            <View style={styles.container}>
                <View style={styles.card}>
                        <Text style={styles.text_title}> Create or join a team </Text>
                        <Pressable style={styles.button} onPress={() => navigation.navigate('Browse')}>
                    <Text style={styles.button_text}>Browse teams </Text>
                  </Pressable>
                  <Pressable style={styles.button} onPress={() => navigation.navigate('Create')} >
                    <Text style={styles.button_text}> Create a team </Text>
                  </Pressable>
                  {/*<Pressable style={styles.button} >*/}
                  {/*  <Text style={styles.button_text}> Cancel </Text>      */}
                  {/*</Pressable>*/}
                </View>
            </View>
        </View>
  );
}

const styles = StyleSheet.create({

    main: {
        flex: 1,
    },

    button: {
    
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 10,
    elevation: 3,
    backgroundColor: '#BF181D',
  },
  button_text: {
    color:'#fff',
    fontSize:15,
  },
  text_title: {
    fontSize: 24,
    textAlign:'center',
  },

  container: {
    backgroundColor: "#C5C5C5",
    flex:1,
    alignItems:'center',
    justifyContent:'center',

  },

  card: {
    flexDirection:'column',
    justifyContent: 'space-around',
    alignItems:'center',
    height: "50%",
    width: "80%",
    backgroundColor: "#fff",
    borderRadius: 15,
    padding: 10,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5, 
  },

});
