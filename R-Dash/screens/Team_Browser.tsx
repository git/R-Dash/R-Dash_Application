import {  FlatList,  StyleSheet, Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { SearchBar } from '@rneui/base';
import React, { useState, useEffect } from 'react';
import { TEAMS } from '../stub/stub';
import TeamListItem from '../components/TeamCmp';
import { Team } from '../core/Team';
import { useDispatch, useSelector } from 'react-redux';
import { getTeamsList } from "../redux/actions/teams";


export default function Team_Browser(props: { navigation: any }) {
    const { navigation } = props;
    const [search, setSearch] = useState('');

    const handlePress = (item: Team) => {
      setSearch('');
      navigation.navigate('Info', { "": item }); // A revoir
    };

    const filteredData = search !== '' ? TEAMS.filter((item) =>
        item.getName().toLowerCase().includes(search.toLowerCase())
    ) : TEAMS;

    const nList = useSelector(state => state.appReducer.teams);

    const dispatch = useDispatch();

    useEffect(() => {
        const loadTeams = async () => {
            await dispatch(getTeamsList());
        };
        loadTeams();
    }, [dispatch]);

    
  return (
    <SafeAreaView style={styles.container}>
        <View style={styles.card}>
          <Text style={styles.text_title}> Browse teams </Text>
          <SearchBar
            platform="default"
            lightTheme
            value={search}
            onChangeText={setSearch}
          />
          <FlatList
            data={nList}
            renderItem={({ item }) => <TeamListItem team={item} onPress={handlePress}/>} />
        </View>
        
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#C5C5C5',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },

  search: {
    backgroundColor:'#fff',
    lightTheme: 1,
  },

  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 10,
    elevation: 3,
    backgroundColor: '#BF181D',
  },

  button_text: {
    color:'#fff',
    fontSize:15,
  },

  text_title: {
    fontSize: 24,
    textAlign:'center',
    padding:10,
  },

  container: {
    backgroundColor: "#C5C5C5",
    flex:1,
    justifyContent:'center',
    alignItems:'center'

  },

  card: {
    flexDirection:'column',
    height: "80%",
    width: "80%",
    backgroundColor: "#fff",
    borderRadius: 15,
    padding: 10,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5, 
  },

});
