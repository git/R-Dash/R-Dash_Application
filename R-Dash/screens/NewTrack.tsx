import React, { useState } from 'react';
import { Pressable, StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import TopBar from '../components/TopBar';
import { addXlsFile } from '../redux/actions/sessions';
import { useDispatch } from 'react-redux';
import * as DocumentPicker from 'expo-document-picker';
import { uploadFiles, DocumentDirectoryPath } from 'react-native-fs';

export default function NewTrack(props: { navigation: any }) {
    const { navigation } = props;
    const dispatch = useDispatch();

    const [pickedDocument, setPickedDocument] = useState<DocumentPicker.DocumentResult | null>(null);
    const [trackName, setTrackName] = useState('');
    const [sessionName, setSessionName] = useState('');

    const handlePickDocument = async () => {
        try {
            const result = await DocumentPicker.getDocumentAsync({});
            if (result.type === 'success') {
                setPickedDocument(result);
            }
            else if(result.type === 'cancel'){
                console.log("AAA");
                setPickedDocument(null);
            }
        } catch (err) {
            console.log(err);
        }
    };

    // var files = [
    //     {
    //       name: "file",
    //       filename: "file.jpg",
    //       filepath: pickedDocument.uri,
    //       filetype: "image/jpeg",
    //     },
    //   ];
   

    // const handleConfirm = async () => {
    //     if (!pickedDocument || !trackName || !sessionName) {
    //         return;
    //     }
    //     const formData = new FormData();
    //     formData.append('file', {
    //         uri: pickedDocument.uri,
    //         type: pickedDocument.type,
    //         name: pickedDocument.name,
    //     });
    //     try {
    //         await dispatch(addXlsFile(formData, 'test_PILOTE', 'test@gmail.com', 'test123', sessionName, trackName, 'Training'));
    //         navigation.goBack();
    //     } catch (error) {
    //         console.log('Error - POST FILE', error);
    //     }
    // };
    // const handleConfirm = async () => {
    //     if (!pickedDocument || !trackName || !sessionName) {
    //         return;
    //     }
    //     try {
    //         const file = new File([await pickedDocument.uri], pickedDocument.name, { type: pickedDocument.type });
    //         const url = 'https://r-dash.azurewebsites.net/File?pseudoPilote=test_PILOTE&nameSession=weekend&nameCircuit=test_CIRCUIT&typeSession=Training';
    //         const options = {
    //             method: 'POST',
    //             body: file,
    //             headers: {
    //                 'Content-Type': 'application/octet-stream',
    //             },
    //         };
    //         await fetch(url, options);
    //         navigation.goBack();
    //     } catch (error) {
    //         console.log('Error - POST FILE', error);
    //     }
    // };
    // const handleConfirm = async () => {
    //     if (!pickedDocument || !trackName || !sessionName) {
    //         return;
    //     }
    //     try {
    //         const file = new File([await pickedDocument.uri], pickedDocument.name, { type: pickedDocument.type });
    //         const url = 'https://r-dash.azurewebsites.net/File?pseudoPilote=test_PILOTE&nameSession=weekend&nameCircuit=test_CIRCUIT&typeSession=Training';
    //         const options = {
    //             method: 'POST',
    //             body: file,
    //             headers: {
    //                 'Content-Type': 'application/octet-stream',
    //             },
    //         };
    //         const response = await fetch(url, options);
    //         const responseData = await response.text(); // or response.text() or response.blob() depending on the expected response type
    //         navigation.goBack();
    //         console.log(responseData);
    //     } catch (error) {
    //         console.log('Error - POST FILE', error);
    //     }
    // };
    const handleConfirm = async () => {
        if (!pickedDocument || !trackName || !sessionName) {
            return;
        }
        try {
            //const file = new File([await pickedDocument.uri], pickedDocument.name, { type: pickedDocument.type });
            const url = 'https://r-dash.azurewebsites.net/File?pseudoPilote=test_PILOTE&nameSession=test%20import&nameCircuit=test_CIRCUIT&typeSession=Training';
            const formData = new FormData();
            console.log(pickedDocument.type);
            console.log(pickedDocument.uri);
            formData.append('file', 
            {
                name: pickedDocument.name,
                type: "application/vnd.ms-excel",
                uri : pickedDocument.uri,

            });
            
            const response = await fetch(url, {
                method: 'POST',
                body: formData,
                headers: {
                    'accept':'*/*',
                    'Content-Type': 'multipart/form-data',
                },
            });
            const data = await response.json();
            console.log('API response:', data);
            if (!response.ok) {
                throw new Error( JSON.stringify(response) + 'Failed to upload file');
            }
    

            navigation.goBack();
        } catch (error) {
            console.log('Error - POST FILE', error);
        }
    };
    
    
    return (
        <SafeAreaView>
            <View style={styles.container}>
                {/* Header */}
                <TopBar navigation={navigation} />

                {/* Page */}
                <View style={{ alignItems: 'center', width: "100%", height: "80%", justifyContent: 'space-around' }}>
                    <View style={styles.page}>
                        <View style={styles.placement}>
                            <Text style={{ paddingTop: 20 }}>Track name: </Text>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={setTrackName}
                                value={trackName}
                                placeholder="Track name"
                            />
                        </View>

                        <View style={styles.placement}>
                            <Text style={{ paddingTop: 20 }}>Session name: </Text>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={setSessionName}
                                value={sessionName}
                                placeholder="Session name"
                            />
                        </View>

                        <View style={styles.placement}>
                            <Text style={{ paddingTop: 30 }}>Import file: </Text>
                            <Pressable onPress={handlePickDocument}>
                                <Image style={styles.import} source={require('../assets/images/import.png')} />
                                <Text>
                                    Picked document: {pickedDocument ? pickedDocument.name : 'none'}
                                </Text>
                            </Pressable>
                        </View>

                    </View>
                </View>

                <View style={styles.placement}>
                    <Pressable style={styles.button} onPress={() => navigation.goBack()}>
                        <Image
                            style={styles.return}
                            source={require('../assets/images/return.png')}
                        />
                    </Pressable>
                    <Pressable style={styles.button} onPress={handleConfirm}>
                        <Image
                            style={styles.return}
                            source={require('../assets/images/checked.png')}
                        />
                    </Pressable>
                </View>
            </View>
        </SafeAreaView>
    );
}
const styles = StyleSheet.create({
    backgroundImage: {
    },

    container: {
        flexDirection: 'column',

    },

    accountSettings: {
        width: '100%',
        height: '100%',
    },
    accountContainerButton: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    ImageContainer: {
        backgroundColor: '#fff',
        borderRadius: 50,
        width: 40,
        height: 40,
        overflow: 'hidden',
        marginRight: 10,
    },
    accountImage: {
        width: '100%',
        height: '100%',
    },
    topbar: {
        backgroundColor: '#BF181D',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        height: 80,
    },
    accountName: {
        color: '#fff',
        fontSize: 20,
    },
    topbarRight: {
        flex: 1,
        alignItems: 'flex-end',
    },
    text_title: {
        flex: 1,
        alignItems: 'flex-start',
        fontWeight: 'bold',
        fontSize: 30,
        textAlign: 'center',
        padding: 10,
    },
    iconContainer: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        backgroundColor: 'transparent',
        width: 24,
        height: 24,
    },
    textInput: {
        alignItems: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        elevation: 3,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#000',
        width: '90%',
    },
    import: {
        width: 70,
        height: 70,
        paddingLeft: 20,
    },
    placement: {
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    page: {
        padding: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
        height: "60%",
        width: "80%",
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 10,
        elevation: 3,
        backgroundColor: '#BF181D',
    },
    return: {
        width: 30,
        height: 30,
    }
});