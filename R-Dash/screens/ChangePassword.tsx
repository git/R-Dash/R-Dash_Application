import React from 'react';
import { Pressable } from 'react-native';
import { StyleSheet, Text, View, Image, TextInput } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function ChangePassword(props: { navigation: any }) {
    const { navigation } = props;

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.maincard}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ paddingBottom: 10, fontSize: 30 }}>Change Password</Text>
                    <TextInput
                        style={styles.textInput}
                        secureTextEntry={true}
                        placeholder="Current password"
                    />
                    <Text></Text>
                    <TextInput
                        style={styles.textInput}
                        secureTextEntry={true}
                        placeholder="New password"
                    />
                    <Text></Text>
                    <TextInput
                        style={styles.textInput}
                        secureTextEntry={true}
                        placeholder="Confirm password"
                    />
                </View>

                <View style={styles.placement}>
                    <Pressable style={styles.button} onPress={() => navigation.goBack() }>
                        <Image
                            style={styles.return}
                            source={require('../assets/images/return.png')}
                        />
                    </Pressable>
                    <Pressable style={styles.button} onPress={() => navigation.goBack()} >
                        <Image
                            style={styles.return}
                            source={require('../assets/images/checked.png')}
                        />
                    </Pressable>
                </View>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#C5C5C5',
        alignItems: 'center',
        justifyContent: 'center',
    },
    maincard: {
        height: "50%",
        width: "80%",
        backgroundColor: "#fff",
        borderRadius: 15,
        padding: 10,
        elevation: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        justifyContent: 'space-evenly',

    },
    button: {

        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 10,
        elevation: 3,
        backgroundColor: '#BF181D',
    },
    placement: {
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    textInput: {
        alignItems: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        elevation: 3,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#000',
        width: '80%',
    },
    return: {
        width: 30,
        height: 30,
    }
});
