import { Member } from "../../core/Member";
import { Owner } from "../../core/Owner";
import { Team } from "../../core/Team";
import { User } from "../../core/User";
import { WaitingMember } from "../../core/WaitingMember";
import { FETCH_USERS } from "../Constants";
import { DtoUserEcurie } from "../dto/dtoUserEcurie";

export const setUsersList = (usersList: User[]) => {
    return {
        type: FETCH_USERS,
        payload: usersList,
    };
}

export const getUsersList = (team: Team) => {
    return async dispatch => {
        try {
            const usersPromise = await fetch(server_link+'/Pilotes/'+team);
            const usersListJson = await usersPromise.json();
            const dto: DtoUserEcurie = usersListJson.map(elt => new DtoUserEcurie(elt["owner"], elt["members"], elt["waitingMember"]));
            const usersList: User[] = []
            usersList.push(dto.getOwner())
            dto.getMembers().forEach(element => {
                usersList.push(element);
            });
            dto.getWaitingMember().forEach(element => {
                usersList.push(element)
            });
            dispatch(setUsersList(usersList)); 
        } catch (error) {
            console.log('Error---------', error);
            //dispatch(fetchDataRejected(error))
        }
    }
}