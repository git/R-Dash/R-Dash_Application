import { Alert } from "react-native";
import { Geocalisation } from "../../core/Geocalisation";
import { Lap } from "../../core/Lap";
import { Point } from "../../core/Point";
import { Session } from "../../core/Session";
import { User } from "../../core/User";
import { FETCH_SESSIONS, server_link } from "../Constants";

export const setSessionsList = (sessionsList: Session[]) => {
    return {
        type: FETCH_SESSIONS,
        payload: sessionsList,
    };
}

// export const addXlsFile = async (file: File) => {
//     try {
//         const formData = new FormData();
//         formData.append('file', file);
//         const response = await fetch(
//             'https://r-dash.azurewebsites.net/File?' + "pseudoPilote=test_PILOTE" + "&Email=test@gmail.com" + "&password=test123" + "&nameSession=test_SESSION" + "&nameCircuit=test_CIRCUIT" + "&typeSession=Unknown", {
//             method: 'POST',
//             body: formData
//         });
//         const data = await response.json();
//         return data;
//     } catch (error) {
//         console.log('Error---------', error);
//     }
// };

export const addXlsFile = (file: File, pseudoPilote: string, email: string, password: string, nameSession: string, nameCircuit: string, typeSession: string) => {
    return async dispatch => {
        try {
            const formData = new FormData();
            formData.append('file', file);
            const response = await fetch(
                server_link+`/File?pseudoPilote=${pseudoPilote}&Email=${email}&password=${password}&nameSession=${nameSession}&nameCircuit=${nameCircuit}&typeSession=${typeSession}`, 
                {
                    method: 'POST',
                    body: formData
                }
            );
            const data = await response.json();
            return data;
        } catch (error) {
            console.log('Error - POST FILE', error);
            Alert.alert('Error', 'An error occured while adding a session. (server might be down)');
        }
    }
};
export const getSessionsList = () => {
    return async dispatch => {
        try {
            const sessionsPromise = await fetch(server_link+'/FullSession');
            const sessionsListJson = await sessionsPromise.json();
            const sessionsList: Session[] = sessionsListJson.map(elt => {
                const laps: Lap[] = elt["tours"].map(lap => {
                    const points: Point[] = lap["points"].map(point => {
                        const geo = new Geocalisation(point["longitude"], point["latitude"]);
                        return new Point(geo, point["timer"] , point["distance"], point["nGear"], point["pBrakeF"], point["aSteer"], point["rPedal"], point["gLong"], point["gLat"], point["vCar"]);
                    });
                    return new Lap(lap["numero"], points, lap["temps"]);
                });
                return new Session(elt["name"], laps, elt["type"]);
            });
            dispatch(setSessionsList(sessionsList));
        } catch (error) {
            console.log('Error -- GET SESSIONS', error);
            Alert.alert('Error', 'An error occured while getting sessions. (server might be down)');
            //dispatch(fetchDataRejected(error))
        }
    }
}