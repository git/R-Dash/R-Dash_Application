import { Team } from "../../core/Team";
import { FETCH_TEAMS, ADD_TEAM, server_link } from "../Constants";

export const setTeamsList = (teamsList: Team[]) => {
    return {
        type: FETCH_TEAMS,
        payload: teamsList,
    };
}

export const addNewTeam = (newTeam: Team) => {
    return async dispatch => {
        try {
            const response = await fetch(server_link + '/Ecuries?' + "Email=test@gmail.com" + "&password=test123" + "&pseudoPilote=test_PILOTE", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newTeam)
            });
            const team = await response.json();
            dispatch({
                type: ADD_TEAM,
                payload: team
            });
        } catch (error) {
            console.log('Error---------', error);
            //dispatch(fetchDataRejected(error))
        }
    }
}

export const getTeamsList = () => {
    return async dispatch => {
        try {
            const teamsPromise = await fetch(server_link+'/Ecuries');
            const teamsListJson = await teamsPromise.json();
            const teamsList: Team[] = teamsListJson.map(elt => new Team(elt["name"], elt["owner"], elt["users"], elt["logo"]));
            dispatch(setTeamsList(teamsList));
        } catch (error) {
            console.log('Error---------', error);
            //dispatch(fetchDataRejected(error))
        }
    }
} 