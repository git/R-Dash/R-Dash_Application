import { Member } from "../../core/Member";
import { Owner } from "../../core/Owner";
import { WaitingMember } from "../../core/WaitingMember";

export class DtoUserEcurie {
    private owner: Owner;
    private members: Member[];
    private waitingMember: WaitingMember[];

    constructor(owner: Owner, members: Member[], waitingMember: WaitingMember[]) {
        this.owner = owner;
        this.members = members;
        this.waitingMember = waitingMember;
    }

    getOwner() {
        return this.owner;
    }
    setOwner(owner: Owner) {
        this.owner = owner;
    }

    getMembers() {
        return this.members;
    }
    setMembers(members: Member[]) {
        this.members = members;
    }

    getWaitingMember() {
        return this.waitingMember;
    }
    setWaitingMember(waitingMember: WaitingMember[]) {
        this.waitingMember = waitingMember;
    }

}