import { addXlsFile } from "../actions/sessions";
import { addNewTeam } from "../actions/teams";
import { FETCH_SESSIONS, FETCH_TEAMS, FETCH_USERS, ADD_TEAM, ADD_FILE } from "../Constants";

const initialState = {
    teams: [],
    users: [],
    sessions: [],
};

const appReducer = (state = initialState, action: { type: any; payload: any; }) => {
    switch (action.type) {
        case ADD_TEAM:
            return { ...state, teams: [...state.teams, addNewTeam] };
        case ADD_FILE:
            return { ...state, sessions: [...state.sessions, addXlsFile] };
        case FETCH_TEAMS:
            return { ...state, teams: action.payload };
        case FETCH_USERS:
            return { ...state, users: action.payload };
        case FETCH_SESSIONS:
            return { ...state, sessions: action.payload };
        default:
            return state;
    }
};

export default appReducer;