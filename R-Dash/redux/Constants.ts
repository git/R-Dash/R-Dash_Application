export const FETCH_USERS = 'FETCH_USERS';
export const FETCH_TEAMS = 'FETCH_TEAMS';
export const FETCH_SESSIONS = 'FETCH_SESSIONS';
export const ADD_TEAM = 'ADD_TEAM';
export const ADD_FILE = 'ADD_FILE';
//export const server_link = "https://codefirst.iut.uca.fr/containers/enzojolys-r-dash_container";
export const server_link = "https://r-dash.azurewebsites.net";