import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import appReducer from './reducers/appReducer';

// Reference here all your application reducers
const reducer = {
    appReducer: appReducer,
}

const middleware = getDefaultMiddleware({
    serializableCheck: false, // Disable serializableCheck
    immutableCheck: false
  });
  
  const store = configureStore({
    reducer,
    middleware,
  });
export default store;